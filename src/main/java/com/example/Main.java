package com.example;
/**
 * Created by jerry on 10/02/16.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Main {
    private static Logger _logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        _logger.info("Starting up Main");
        ApplicationContext ctx = SpringApplication.run(Main.class, args);
    }
}
