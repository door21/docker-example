package com.example;
/**
 * Created by jerry on 10/02/16.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Greeter {
    private static Logger _logger = LoggerFactory.getLogger(Greeter.class);
    private String message;

    public Greeter(String msg) {
        message = msg;
    }

    public String getMessage() {
        _logger.debug("Returning message");
        return message;
    }

}
