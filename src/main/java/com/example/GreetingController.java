package com.example;/**
 * Created by jerry on 10/02/16.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    private static Logger _logger = LoggerFactory.getLogger(GreetingController.class);

    @RequestMapping("/greet")
    public Greeter greet() {
        _logger.debug("Received request for greeting");
        return new Greeter("Hello World");
    }
}
